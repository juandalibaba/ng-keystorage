# Añadir autenticación

Protegemos las rutas que queramos con ``CanActivate``

    const routes: Routes = [
    { 
        path: 'admin',
        component: MenuAdminComponent,
        canActivate: [ GuardService ],
        children: [
        {
            path: '',
            canActivateChild: [ GuardService ],
            children: [
            { path: 'usuarios', component: UsuariosComponent},
            { path: 'grupos', component: GruposComponent},
            ]
        }
        ]
    }
    ];

Creamos un servicio guard.service.ts que implementa las interfaces CanActivate y
CanActivateChild, y un servicio auth.service.ts que será el encargado de comprobar si el usuario está autenticado y si no de reenviarlo a un formulario de login.

Para que funcione auth.service.ts hay que importar HttpClientModule en el módulo 
principal.

Creamos un modulo de login con routing y un componente de login en él. Este componente incorpora un formulario de login.

Creamos un servicio para la configuración de la aplicación.

Usamos https://jsonplaceholder.typicode.com/ para crear una API REST fake. Con
esta aplicación instalada creamos un fichero db.json que servirá para que el
servicio web fake pille los datos.

Funcionamiento:

Cuando se alcanza una ruta protegida con canActivate o canActivateChild,
se ejecuta la correpondiente acción del servicio GuardService. En el caso
de ejemplo ambas acciones hacen lo mismo: comprueban si el usuario ya está
loggedIn, si lo está devuelve un true y el contenido del componente cuya
ruta está protegida se muestra. Si no lo está se guarda la URL protegida 
(para usarla después cuando se haya loggedIn) y se redirige a la ruta /login.

El componente LoginComponent muestra un formulario de login. Cuando el usuario
envia las credenciales se subscribe al observable checkLogin del servicio AuthService, el cual realiza una petición a la API para comprobar si el usuario
es válido. En caso de que lo sea coloca el atributo isLoggedIn del servicio
AuthService a true y redirige a la ruta protegida desde donde se inicio todo el
proceso. En caso negativo muestra un mensaje de errro. 




