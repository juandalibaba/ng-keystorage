import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotFoundComponent } from './not-found/not-found.component';
import { AdminRoutingModule } from './admin/admin-routing.module';
import { KeystorageRoutingModule } from './keystorage/keystorage-routing.module';
import { PublicRoutingModule } from './public/public-routing.module';
import { LoginRoutingModule } from './login/login-routing.module';

const routes: Routes = [
  { path: "**", component: NotFoundComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, /* { enableTracing: true } */),
    AdminRoutingModule,
    KeystorageRoutingModule,
    PublicRoutingModule,
    LoginRoutingModule
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
