import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../model/user';
import { ConfigService } from './config.service';
import { handleError } from './handleError';
import { catchError } from 'rxjs/operators';

@Injectable()
export class UsersService {
  private url: string
  private httpOptions
  constructor(
    private httpClient: HttpClient,
    private config: ConfigService) {
    this.url = `${this.config.apiUrl}/users`
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    }
  }

  getAll(): Observable<User[]> {
    // Atención, hasta que no haya una subscripción no se realiza la petición
    return this.httpClient.get<User[]>(this.url, this.httpOptions)
      .pipe(
        catchError(handleError('Error al obtener los usuarios'))
      )
  }

  post(user: User): Observable<User> {
    let data = JSON.stringify(user)
    return this.httpClient.post<User>(this.url, data, this.httpOptions)
      .pipe(
        catchError(handleError('Error al crear el usuario'))
      )
  }

  put(user: User): Observable<User> {
    let data = JSON.stringify(user)
    let url = `${this.url}/${user.id}`
    return this.httpClient.put<User>(url, data, this.httpOptions)
      .pipe(
        catchError(handleError('Error al actualizar el usuario'))
      )
  }

  delete(user: User): Observable<User> {
    let url = `${this.url}/${user.id}`
    return this.httpClient.delete<User>(url, this.httpOptions)
      .pipe(
        catchError(handleError('Error al borrar el usuario'))
      )
  }
}
