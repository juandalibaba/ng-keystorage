import { HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

export function handleError(msg: string) {
    return function (error: HttpErrorResponse) {
        let messageError: string
        this.isLoggedIn = false
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            messageError = 'An error occurred:', error.error.message
            console.error(messageError);
            return Observable.throw(messageError)
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.error(
                `Backend returned code ${error.status}, ` +
                `body was: ${error.error}`);
            return Observable.throw(msg)
        }
    }

};