import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Group } from '../model/group';
import { ConfigService } from './config.service';
import { handleError } from './handleError';
import { catchError } from 'rxjs/operators';

@Injectable()
export class GroupsService {
  private url: string
  private httpOptions

  constructor(
    private httpClient: HttpClient,
    private config: ConfigService) {
    this.url = `${this.config.apiUrl}/groups`
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    }
  }

  getAll(): Observable<Group[]> {
    // Atención, hasta que no haya una subscripción no se realiza la petición
    return this.httpClient.get<Group[]>(this.url, this.httpOptions)
      .pipe(
        catchError(handleError('Error al obtener los grupos'))
      )
  }

  post(group: Group): Observable<Group> {
    let data = {
      name: group.name,
    }
    return this.httpClient.post<Group>(this.url, data, this.httpOptions)
      .pipe(
        catchError(handleError('Error al crear el grupo'))
      )
  }

  put(group: Group): Observable<Group> {
    let data = {
      name: group.name
    }
    let url = `${this.url}/${group.id}`
    return this.httpClient.put<Group>(url, data, this.httpOptions)
      .pipe(
        catchError(handleError('Error al actualizar el grupo'))
      )
  }

  delete(group: Group): Observable<Group> {
    let url = `${this.url}/${group.id}`
    return this.httpClient.delete<Group>(url, this.httpOptions)
      .pipe(
        catchError(handleError('Error al borrar el grupo'))
      )
  }
}
