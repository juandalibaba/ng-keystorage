import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { ConfigService } from './config.service';
import { catchError, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { TokenInterface } from '../interfaces/token.interface';

@Injectable()
export class AuthService {

  isLoggedIn =  false
  redirectUrl = ""

  constructor(
    private httpClient: HttpClient,
    private config: ConfigService) { }

  checkUser(username: string, password: string): Observable<TokenInterface> {
    const url = `${this.config.apiUrl}/token`
    const data = {
      username: username,
      password: password
    }
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    }

    // Atención, hasta que no haya una subscripción no se realiza la petición
    return this.httpClient.post<TokenInterface>(url, data, httpOptions)
      .pipe(
        tap(val => this.isLoggedIn = true),
        catchError(this.handleError)
      )
  }

  private handleError(error: HttpErrorResponse) {
    let messageError: string
    this.isLoggedIn = false
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      messageError = 'An error occurred:', error.error.message
      console.error(messageError);
      return Observable.throw(messageError)
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
        return Observable.throw('No está autorizado. Registrese si eso')
    }
  };

}
