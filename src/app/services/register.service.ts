import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../model/user';
import { ConfigService } from './config.service';
import { Observable } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';
import { handleError } from './handleError';

@Injectable()
export class RegisterService {

  constructor(
    private httpClient: HttpClient,
    private config: ConfigService) { }

  private id() {
    return '_' + Math.random().toString(36).substr(2, 9);
  };

  register(user: User, cleanPassword: string): Observable<User> {
    const url = `${this.config.apiUrl}/users`
    const data = {
      id: this.id(),
      username: user.username,
      email: user.email
    }
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    }

    // Atención, hasta que no haya una subscripción no se realiza la petición
    return this.httpClient.post<User>(url, data, httpOptions)
      .pipe(
        catchError(handleError('Error al crear el usuario'))
      )

  }

}
