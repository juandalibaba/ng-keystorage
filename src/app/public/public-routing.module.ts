import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MenuPublicComponent } from './menu-public/menu-public.component';
import { RegisterComponent } from './register/register.component';
import { LandingpageComponent } from './landingpage/landingpage.component';

const routes: Routes = [
  {
    path: '',
    component: MenuPublicComponent,
    children: [
      { path: 'register', component: RegisterComponent },
      { path: '', component: LandingpageComponent },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicRoutingModule { }
