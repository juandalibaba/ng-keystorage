import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { User } from '../../model/user';
import { RegisterService } from '../../services/register.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  user: User = new User()
  cleanPassword: string
  cleanPasswordConfirm: string
  message: string
  errorMessage: string

  constructor(private registerService: RegisterService) { }

  ngOnInit() {
    
  }

  register() {
    //console.log(this.user)
    this.registerService.register(this.user, this.cleanPassword).subscribe(
      (user: User) => {
        this.message = "Enhorabuena ya eres usuario de KeyStorage"
      },
      error => {
        this.errorMessage = error
      })
  }

}
