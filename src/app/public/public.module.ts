import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PublicRoutingModule } from './public-routing.module';
import { RegisterComponent } from './register/register.component';
import { LandingpageComponent } from './landingpage/landingpage.component';
import { MenuPublicComponent } from './menu-public/menu-public.component';
import { FormsModule } from '@angular/forms';
import { HighlightDirective } from '../directives/highlight.directive';
import { ValidateEqualDirective } from '../directives/validate-equal.directive';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    PublicRoutingModule,
    FormsModule,
    NgbModule
    
  ],
  declarations: [
    RegisterComponent,
    LandingpageComponent,
    MenuPublicComponent,
    HighlightDirective, 
    ValidateEqualDirective
  ]
})
export class PublicModule { }
