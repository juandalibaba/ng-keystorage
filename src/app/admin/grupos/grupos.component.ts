import { Component, OnInit } from '@angular/core';
import { GroupsService } from '../../services/groups.service';
import { Group } from '../../model/group';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-grupos',
  templateUrl: './grupos.component.html',
  styleUrls: ['./grupos.component.css']
})
export class GruposComponent implements OnInit {

  groups: Group[]
  activeGroup: Group = new Group()
  editMode: boolean = false
  message = ""
  errorMessage = ""

  constructor(
    private groupService: GroupsService,
    private modalService: NgbModal) { }

  ngOnInit() {
    this.list()
  }

  list() {
    this.groupService.getAll().subscribe(
      (groups: Group[]) => {
        console.log(groups)
        this.groups = groups
      },
      error => {
        console.log(error)
      }
    )
  }

  delete(group: Group) {
    console.log(group)
    this.activeGroup = group
    this.groupService.delete(group).subscribe(
      (group: Group) => {
        let index = this.groups.indexOf(this.activeGroup);
        if (index !== -1) this.groups.splice(index, 1);
      }
    )
  }

  showForm(group?: Group) {
    this.activeGroup = (group != undefined) ? group : new Group()
    this.editMode = true
  }

  createOrUpdate() {
    let operation = (this.activeGroup.id == undefined) ? 'post' : 'put'
    let verboOper = (operation == 'post')? 'añadido' : 'modificado'
    this.groupService[operation](this.activeGroup).subscribe(
      (group: Group) => {
        if (operation == 'post') {
          this.groups.push(group)
        } else {
          this.activeGroup = group
        }
        this.editMode = false
        this.setMessage(`El grupo "${this.activeGroup.name}" ha sido ${verboOper}`)
        
      },
      error => {
        console.log(error)
      }
    )
  }

  confirmDelete(group: Group, modal) {
    this.activeGroup = group
    this.modalService.open(modal).result.then((result) => {
      if (result == 'delete') this.delete(group)
    }, (reason) => { })
  }

  setMessage(m: string){
    this.message = m
    setTimeout(() => this.message = "", 3000)
  }
}
