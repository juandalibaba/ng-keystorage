import { Component, OnInit } from '@angular/core';
import { UsersService } from '../../services/users.service';
import { User } from '../../model/user';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Group } from '../../model/group';
import { GroupsService } from '../../services/groups.service';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.css']
})
export class UsuariosComponent implements OnInit {

  users: User[]
  groups: Group[]
  activeUser: User = new User()
  editMode: boolean = false
  message = ""
  errorMessage = ""

  constructor(
    private userService: UsersService,
    private groupService: GroupsService,
    private modalService: NgbModal) { }

  ngOnInit() {
    this.list()
    this.getGroups()
  }

  getGroups(){
    this.groupService.getAll().subscribe(
      (groups: Group[]) => {
        this.groups = groups
      },
      error => {}
    )
  }

  list() {
    this.userService.getAll().subscribe(
      (users: User[]) => {
        console.log(users)
        this.users = users
      },
      error => {
        console.log(error)
      }
    )
  }

  delete(user: User) {
    console.log(user)
    this.activeUser = user
    this.userService.delete(user).subscribe(
      (user: User) => {
        let index = this.users.indexOf(this.activeUser);
        if (index !== -1) this.users.splice(index, 1);
      }
    )
  }

  showForm(user?: User) {
    this.activeUser = (user != undefined) ? user : new User()
    this.editMode = true
  }

  createOrUpdate() {
    console.log(this.activeUser)
    let operation = (this.activeUser.id == undefined) ? 'post' : 'put'
    let verboOper = (operation == 'post')? 'añadido' : 'modificado'
    this.userService[operation](this.activeUser).subscribe(
      (user: User) => {
        if (operation == 'post') {
          this.users.push(user)
        } else {
          this.activeUser = user
        }
        this.editMode = false
        this.setMessage(`El usuario "${this.activeUser.username}" ha sido ${verboOper}`)
        
      },
      error => {
        console.log(error)
      }
    )
  }

  confirmDelete(user: User, modal) {
    this.activeUser = user
    this.modalService.open(modal).result.then((result) => {
      if (result == 'delete') this.delete(user)
    }, (reason) => { })
  }

  setMessage(m: string){
    this.message = m
    setTimeout(() => this.message = "", 3000)
  }

  // Esta función es necesaria para que las opciones del select (los grupos) sean
  // seleccionadas cuando el usuario pertenezca a los grupos en cuestión.
  // Se usa como binding desde el modelo a la plantilla en el elemento <select>
  compareGroup(g1: Group, g2: Group){
    return g1.id == g2.id
  }
}
