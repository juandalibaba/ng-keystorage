import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MenuAdminComponent } from './menu-admin/menu-admin.component';
import { UsuariosComponent } from './usuarios/usuarios.component';
import { GruposComponent } from './grupos/grupos.component';
import { GuardService } from '../services/guard.service';

const routes: Routes = [
  {
    path: 'admin',
    component: MenuAdminComponent,
    canActivate: [ GuardService ],
    children: [
      { path: 'usuarios', component: UsuariosComponent },
      { path: 'grupos', component: GruposComponent },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
