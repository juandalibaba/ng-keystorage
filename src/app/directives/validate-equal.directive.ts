import { Directive, Input } from '@angular/core';
import { NG_VALIDATORS, Validator, AbstractControl, ValidatorFn } from '@angular/forms';


@Directive({
  selector: '[appValidateEqual]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: ValidateEqualDirective,
      multi: true
    }]
})
export class ValidateEqualDirective implements Validator {
  @Input('appValidateEqual') validateEqual: string;

  validate(control: AbstractControl): { [key: string]: any } | null {
    // control.value es el valor del elemento que se pasa al validador
    // es decir el que vaya en cleanpasswordconfirm, que es donde se
    // está usando dicho validador
    // control.root.get(this.validateEqual).value, es el valor introducido
    // en el input text cleanpassword, la función get es la que busca en el
    // DOM 
    
    let passwordConfirmValue = control.value
    let passwordValue = control.root.get(this.validateEqual).value
    if(passwordConfirmValue != passwordValue){
      return { appValidateEqual: true}
    }
    return null
  }

  constructor() { }

}
