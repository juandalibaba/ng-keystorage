import { Directive, ElementRef } from '@angular/core';
import { style } from '@angular/animations';

@Directive({
  selector: '[appHighlight]'
})
export class HighlightDirective {

  constructor(el: ElementRef) { 
    el.nativeElement.style.backgroundColor = 'yellow'
  }

}
