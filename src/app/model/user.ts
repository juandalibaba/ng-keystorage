enum Rol {
    user = "user",
    admin = "admin"
}

export class User {
    
    id: number
    username: string
    email: string
    rol: Rol
    groups: User[]

    constructor() {
        this.rol = Rol.user
    }

    promote() {
        this.rol = Rol.admin
    }
}