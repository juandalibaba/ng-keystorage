import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

  @ViewChild('content') content: ElementRef

  constructor(
    private authService: AuthService,
    private router: Router,
    private modalService: NgbModal
  ) { }

  ngOnInit() {

  }

  confirm() {
    this.modalService.open(this.content).result.then((result) => {
      console.log('result')
      console.log(result)
      if (result == 'logout') {
        this.authService.isLoggedIn = false
        this.router.navigate([""])
      }
    }, (reason) => {
      console.log('reason')
      console.log(reason)
    })
  }
}
