import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { TokenInterface } from '../../interfaces/token.interface';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username: string
  password: string
  errorMessage = ""

  @ViewChild('loginForm') loginForm: NgForm

  constructor(private router: Router, private authService: AuthService) { }

  ngOnInit() {
    console.log(this.loginForm)
  }

  checkUser() {
    // Atención hasta que no se hace el subscribe no se realiza la petición
    this.authService.checkUser(this.username, this.password).subscribe(
      (token: TokenInterface) => {
        console.log(token)
        this.errorMessage = ""
        this.router.navigate([this.authService.redirectUrl]);
      },
      error => {
        this.errorMessage = error
        console.log(error)
      }
    )

  }
}
