import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { AdminModule } from './admin/admin.module';
import { KeystorageModule } from './keystorage/keystorage.module';
import { PublicModule } from './public/public.module';
import { NotFoundComponent } from './not-found/not-found.component';
import { GuardService } from './services/guard.service';
import { LoginModule } from './login/login.module';
import { AuthService } from './services/auth.service';
import { ConfigService } from './services/config.service';
import { HttpClientModule } from '@angular/common/http';
import { RegisterService } from './services/register.service';
import { UsersService } from './services/users.service';
import { GroupsService } from './services/groups.service';

@NgModule({
  declarations: [
    AppComponent,
    NotFoundComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NgbModule.forRoot(),
    AppRoutingModule,
    AdminModule,
    KeystorageModule,
    PublicModule,
    LoginModule
  ],
  providers: [
    GuardService,
    AuthService,
    ConfigService,
    RegisterService,
    UsersService,
    GroupsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
