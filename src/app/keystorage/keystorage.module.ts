import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { KeystorageRoutingModule } from './keystorage-routing.module';
import { ServicesComponent } from './services/services.component';
import { KeysComponent } from './keys/keys.component';
import { MenuKeystorageComponent } from './menu-keystorage/menu-keystorage.component';

@NgModule({
  imports: [
    CommonModule,
    KeystorageRoutingModule,    
  ],
  declarations: [ServicesComponent, KeysComponent, MenuKeystorageComponent]
})
export class KeystorageModule { }
