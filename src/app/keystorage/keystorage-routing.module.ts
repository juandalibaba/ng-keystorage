import { NgModule } from '@angular/core';
import { Routes, RouterModule, CanActivate } from '@angular/router';
import { ServicesComponent } from './services/services.component';
import { KeysComponent } from './keys/keys.component';
import { MenuKeystorageComponent } from './menu-keystorage/menu-keystorage.component';
import { GuardService } from '../services/guard.service';

const routes: Routes = [
  { 
    path: 'keystorage',
    component: MenuKeystorageComponent,
    canActivate: [ GuardService ],
    children: [
      { path: 'services', component: ServicesComponent},
      { path: 'keys', component: KeysComponent},
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class KeystorageRoutingModule { }
