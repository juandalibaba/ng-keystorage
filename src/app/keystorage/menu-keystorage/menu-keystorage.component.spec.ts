import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuKeystorageComponent } from './menu-keystorage.component';

describe('MenuKeystorageComponent', () => {
  let component: MenuKeystorageComponent;
  let fixture: ComponentFixture<MenuKeystorageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuKeystorageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuKeystorageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
